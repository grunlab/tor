[![pipeline status](https://gitlab.com/grunlab/tor/badges/main/pipeline.svg)](https://gitlab.com/grunlab/tor/-/commits/main)

# GrunLab Tor

Tor client node non-root container image and deployment on Kubernetes.

Docs:
- https://docs.grunlab.net/images/tor.md
- https://docs.grunlab.net/apps/tor.md

Base image: [grunlab/base-image/debian:12][base-image]

Format: docker

Supported architecture(s):
- arm64

GrunLab project(s) using this service:
- [grunlab/deluge][deluge]
- [grunlab/vigixplorer][vigixplorer]

[base-image]: <https://gitlab.com/grunlab/base-image>
[deluge]: <https://gitlab.com/grunlab/deluge>
[vigixplorer]: <https://gitlab.com/grunlab/vigixplorer>